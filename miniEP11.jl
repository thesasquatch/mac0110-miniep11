function palindromo(string)
    test1 = []
    test2 = []
    d = split(lowercase(string), "")
    for i in eachindex(d)
        if all(isletter, d[i])
            push!(test1, d[i])
        end
    end
    for i in eachindex(test1)
        test1[i] = replace(test1[i], r"ã|á|à|â" => "a")
        test1[i] = replace(test1[i], r"é|ê" => "e")
        test1[i] = replace(test1[i], r"ô|ó|õ" => "o")
        test1[i] = replace(test1[i], r"í" => "i")
        test1[i] = replace(test1[i], r"ú" => "u")
    end
    for i in eachindex(test1)
        push!(test2, test1[length(test1) + 1 - i])
    end
    if test1 == test2
        return true
    else
        return false
    end
end
    
            
    
